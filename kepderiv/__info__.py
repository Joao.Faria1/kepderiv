# -*- coding: utf-8 -*-

# Copyright 2019-2021 Jean-Baptiste Delisle
#
# This file is part of kepderiv.
#
# kepderiv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# kepderiv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with kepderiv.  If not, see <http://www.gnu.org/licenses/>.

__title__ = 'kepderiv'
__description__ = 'Keplerian modeling and derivatives for radial velocities analysis.'
__author__ = 'Jean-Baptiste Delisle'
__author_email__ = 'jean-baptiste.delisle@unige.ch'
__license__ = 'GPLv3'
__url__ = 'https://gitlab.unige.ch/delisle/kepderiv'
__version__ = "1.0.5"
