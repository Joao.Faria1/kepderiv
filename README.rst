kepderiv: Keplerian modeling and derivatives for radial velocity and astrometry analyses
========================================================================================

Read the documentation at `<https://obswww.unige.ch/~delisle/kepderiv/doc/>`_.
